package com.epam.controller;

public interface Controller {

    void showNewOffers();

    void addNewCustomer();

    void showAllCustomers();

    void chooseEvent(String keyMenu);

    void chooseBouquet(String keyMenu);

    void createCustomBouquet(String keyMenu);

    void chooseCustomFlower(String flower);

    void identifyClient(String keyMenu);

    String showOrder();
}
