package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.impl.ModelImpl;
import com.epam.model.impl.client.Client;


public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        this.model = new ModelImpl();
    }

    @Override
    public void showNewOffers() {
        model.showNewOffers();
    }

    @Override
    public void addNewCustomer() {
       model.addNewCustomer();
    }

    @Override
    public void showAllCustomers() {
        for (Client client : Client.getListClients()) {
            System.out.printf("Name of client: %s, id client: %d, type of card: %s.\n", client.getName(), client.getId(), client.getCard().toString());
        }
    }

    @Override
    public void chooseEvent(String keyMenu) {
        model.chooseEvent(keyMenu);
    }

    @Override
    public void chooseBouquet(String keyMenu) {
        model.chooseBouquet(keyMenu);
    }

    @Override
    public void createCustomBouquet(String keyMenu) {
        model.createCustomBouquet(keyMenu);
    }

    @Override
    public void chooseCustomFlower(String flower) {
        model.chooseCustomFlower(flower);
    }

    @Override
    public void identifyClient(String keyMenu) {
        model.identifyClient(keyMenu);
    }

    @Override
    public String showOrder() {
        return model.showOrder();
    }
}
