package com.epam.model;

public interface Model {

    void showNewOffers();

    void addNewCustomer();

    void chooseEvent(String keyMenu);

    void chooseBouquet(String keyMenu);

    void createCustomBouquet(String keyMenu);

    void chooseCustomFlower(String flower);

    String showOrder();

    void identifyClient(String keyMenu);
}
