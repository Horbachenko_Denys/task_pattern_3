package com.epam.model.impl.offer;

import java.util.ArrayList;
import java.util.List;

public class Offer {

    List<String> offers = new ArrayList<>();

    public Offer() {
        this.addOffers();
    }

    private void addOffers() {
        offers.add("from 01/08 to 02/13 enter waiting14 for 20% discount, available only 100 first customers");
        offers.add("from 12/30 to 01/02 enter waitingNY for 20% discount, available only 100 first customers");
        offers.add("from 12/20 to 12/26 enter waitingCE for 20% discount, available only 100 first customers");
        offers.add("from 01/05 to 01/08 enter waitingOE for 20% discount, available only 100 first customers");
    }


   public void showOffers(){
       for (String text : offers) {
           System.out.printf("%d: %s\n", offers.indexOf(text) + 1, text);
       }
   }
}
