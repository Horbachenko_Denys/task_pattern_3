package com.epam.model.impl;

import com.epam.model.Model;
import com.epam.model.impl.client.Client;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.event.EventFactory;
import com.epam.model.impl.offer.Offer;

import java.util.Scanner;

public class ModelImpl implements Model {

    private static Scanner scanner = new Scanner(System.in);

    Client client;
    Offer offer;
    Event event;
    Bouquet bouquet;

    @Override
    public void showNewOffers() {
        offer = new Offer();
        offer.showOffers();
    }

    @Override
    public void addNewCustomer() {
        System.out.println("Enter user name...");
        String name = scanner.nextLine();
        System.out.println("Enter user id...");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter user Card type...(gold, bonus, social or usual)");
        String card = scanner.nextLine();
        client = new Client(name, id, card);
    }

    @Override
    public void chooseEvent(String keyMenu) {
        event = EventFactory.createEvent(keyMenu);
    }

    @Override
    public void chooseBouquet(String keyMenu) {
        bouquet = new Bouquet();
        bouquet.chooseBouquet(event, keyMenu, bouquet, keyMenu);
        System.out.println("Enter client id...");
    }

    @Override
    public void createCustomBouquet(String keyMenu) {
        bouquet = new Bouquet();

    }

    @Override
    public void chooseCustomFlower(String keyMenu) {
        bouquet.chooseBouquet(event, "4", bouquet, keyMenu);
    }

    @Override
    public String showOrder() {
        return "Your order is: " + event.getClass().getSimpleName() + " bouquet, price is: " + bouquet.getPrice() + " UAH.";
    }

    @Override
    public void identifyClient(String keyMenu) {
        int id = Integer.parseInt(keyMenu);
        if (!Client.clients.containsKey(id)){
            addNewCustomer();
        }
        bouquet.setPrice((int)(bouquet.getPrice() * Client.getClients().get(id).getCard().getDiscount()));
    }
}
