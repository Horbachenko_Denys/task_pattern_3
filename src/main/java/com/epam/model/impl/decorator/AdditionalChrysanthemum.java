package com.epam.model.impl.decorator;

import com.epam.model.impl.Bouquet;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.flower.Chrysanthemum;

public class AdditionalChrysanthemum extends Decorator {

    private int additionalPrice = 15;

    public AdditionalChrysanthemum() {
        setAdditionalPrice(this.additionalPrice);
        setAdditionalFlower(new Chrysanthemum());
    }

    @Override
    public void makeCustomBouquet(String keyWord, Bouquet bouquet, Event event) {

    }

    @Override
    public Bouquet getBouquet() {
        return bouquet;
    }
}
