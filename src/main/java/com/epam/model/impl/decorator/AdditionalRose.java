package com.epam.model.impl.decorator;

import com.epam.model.impl.Bouquet;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.flower.Rose;

public class AdditionalRose extends Decorator {

    int additionalPrice = 10;

    public AdditionalRose() {
        setAdditionalPrice(this.additionalPrice);
        setAdditionalFlower(new Rose());
    }

    @Override
    public void makeCustomBouquet(String keyWord, Bouquet bouquet, Event event) {

    }

    @Override
    public Bouquet getBouquet() {
        return bouquet;
    }

}
