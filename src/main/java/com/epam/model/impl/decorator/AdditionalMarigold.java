package com.epam.model.impl.decorator;

import com.epam.model.impl.Bouquet;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.flower.Marigold;

public class AdditionalMarigold extends Decorator {


    private int additionalPrice = 10;

    public AdditionalMarigold() {
        setAdditionalPrice(this.additionalPrice);
        setAdditionalFlower(new Marigold());
    }

    @Override
    public void makeCustomBouquet(String keyWord, Bouquet bouquet, Event event) {

    }

    @Override
    public Bouquet getBouquet() {
        return bouquet;
    }

}
