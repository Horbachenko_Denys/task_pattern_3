package com.epam.model.impl.decorator;

import com.epam.model.impl.Bouquet;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.flower.Tulip;

public class AdditionalTulip extends Decorator {

    int additionalPrice = 10;

    public AdditionalTulip() {
        setAdditionalPrice(this.additionalPrice);
        setAdditionalFlower(new Tulip());
    }

    @Override
    public void makeCustomBouquet(String keyWord, Bouquet bouquet, Event event) {

    }

    @Override
    public Bouquet getBouquet() {
        return bouquet;
    }

}
