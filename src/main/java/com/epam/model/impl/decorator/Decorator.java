package com.epam.model.impl.decorator;

import com.epam.model.impl.Bouquet;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.flower.Flower;

import java.util.List;

public abstract class Decorator extends Bouquet implements Event {

    private Event event;
    private int additionalPrice;
    private Flower additionalFlower;
    Bouquet bouquet;

    @Override
    public void makeSimpleBouquet(Bouquet bouquet) {
    }

    @Override
    public void makeOrdinaryBouquet(Bouquet bouquet) {

    }

    @Override
    public void makeHugeBouquet(Bouquet bouquet) {

    }

    public void setEvent(Event outEvent, Bouquet outBouquet) {
        this.bouquet = outBouquet;
        this.event = outEvent;
        if (additionalFlower != null) {
            event.getBouquet().getFlowers().add(additionalFlower);
            event.getBouquet().addPrice(additionalPrice);
        }
    }

    public void setAdditionalPrice(int additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setAdditionalFlower(Flower additionalFlower) {
        this.additionalFlower = additionalFlower;
    }

    @Override
    public int getPrice() {
        return bouquet.getPrice();
    }

    @Override
    public List<Flower> getFlowers() {
        return bouquet.getFlowers();
    }
}
