package com.epam.model.impl.event;

import com.epam.model.impl.Bouquet;

public interface Event {


    void makeSimpleBouquet(Bouquet bouquet);
    void makeOrdinaryBouquet(Bouquet bouquet);
    void makeHugeBouquet(Bouquet bouquet);
    void makeCustomBouquet(String keyWord, Bouquet bouquet, Event event);
     Bouquet getBouquet();
}
