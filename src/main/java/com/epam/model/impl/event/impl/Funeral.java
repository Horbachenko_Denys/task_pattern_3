package com.epam.model.impl.event.impl;

import com.epam.model.impl.Bouquet;
import com.epam.model.impl.decorator.AdditionalChrysanthemum;
import com.epam.model.impl.decorator.AdditionalMarigold;
import com.epam.model.impl.decorator.AdditionalRose;
import com.epam.model.impl.decorator.AdditionalTulip;
import com.epam.model.impl.event.Event;
import com.epam.model.impl.flower.Flower;
import com.epam.model.impl.flower.Marigold;
import com.epam.model.impl.flower.Rose;

public class Funeral implements Event {

    Bouquet bouquet;

    @Override
    public void makeSimpleBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        for (int i = 0; i < 10; i++) {
            Flower flowerMariglod = new Marigold();
            bouquet.getFlowers().add(flowerMariglod);
            bouquet.addPrice(flowerMariglod.getPrice());
        }
    }

    @Override
    public void makeOrdinaryBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        for (int i = 0; i < 15; i++) {
            Flower flowerMariglod = new Marigold();
            bouquet.getFlowers().add(flowerMariglod);
            bouquet.addPrice(flowerMariglod.getPrice());
        }
        for (int i = 0; i < 5; i++) {
            Flower flowerRose = new Rose();
            bouquet.getFlowers().add(flowerRose);
            bouquet.addPrice(flowerRose.getPrice());
        }
    }

    @Override
    public void makeHugeBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        for (int i = 0; i < 30; i++) {
            Flower flowerMariglod = new Marigold();
            bouquet.getFlowers().add(flowerMariglod);
            bouquet.addPrice(flowerMariglod.getPrice());
        }
        for (int i = 0; i < 10; i++) {
            Flower flowerRose = new Rose();
            bouquet.getFlowers().add(flowerRose);
            bouquet.addPrice(flowerRose.getPrice());
        }
    }

    @Override
    public void makeCustomBouquet(String keyWord, Bouquet bouquet, Event event) {
        this.bouquet = bouquet;
        if (keyWord.equals("1")) {
            AdditionalChrysanthemum additionalChrysanthemum = new AdditionalChrysanthemum();
            additionalChrysanthemum.setEvent(event, bouquet);
        }
        if (keyWord.equals("2")) {
            AdditionalMarigold additionalMarigold = new AdditionalMarigold();
            additionalMarigold.setEvent(event, bouquet);
        }
        if (keyWord.equals("3")) {
            AdditionalRose additionalRose = new AdditionalRose();
            additionalRose.setEvent(event, bouquet);
        }
        if (keyWord.equals("4")) {
            AdditionalTulip additionalTulip = new AdditionalTulip();
            additionalTulip.setEvent(event, bouquet);
        }
    }

    @Override
    public Bouquet getBouquet() {
        return bouquet;
    }
}
