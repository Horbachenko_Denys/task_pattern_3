package com.epam.model.impl.event;

import com.epam.model.impl.event.impl.Birthday;
import com.epam.model.impl.event.impl.Funeral;
import com.epam.model.impl.event.impl.StValentinesDay;
import com.epam.model.impl.event.impl.Wedding;

public class EventFactory {

    private static Event event;

    public static Event createEvent(String keyMenu) {
        if (keyMenu.equals("1")) {
            event = new Wedding();
        }
        if (keyMenu.equals("2")) {
            event = new Birthday();
        }
        if (keyMenu.equals("3")) {
            event = new StValentinesDay();
        }
        if (keyMenu.equals("4")) {
            event = new Funeral();
        }
        return event;
    }
}
