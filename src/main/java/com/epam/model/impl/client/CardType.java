package com.epam.model.impl.client;

public enum CardType {
    GOLD(0.7), BONUS(0.85), SOCIAL(0.8), USUAL(1.0);

    private double discount;

    CardType(double discount) {
        this.discount = discount;
    }

    public double getDiscount() {
        return discount;
    }
}
