package com.epam.model.impl.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Client {

    public static Map<Integer, Client> clients = new HashMap<>();
    private String name;
    private int id;
    private Card card;

    public Client(String name, int id, String card) {
        if (!clients.containsKey(id)){
        this.setName(name);
        this.setId(id);
        this.setCard(new Card(card));
        clients.put(id, this);
        }
        else System.out.println("Existing client, try another id.");
    }

    public static List<Client> getListClients() {
        List<Client> clientsList = new ArrayList<>();
        clients.forEach((key, value) -> clientsList.add(value));
        return clientsList;
    }

    public static Map<Integer, Client> getClients() {
        return clients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CardType getCard() {
        return card.cardType;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
