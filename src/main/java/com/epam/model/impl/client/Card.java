package com.epam.model.impl.client;

public class  Card {

    CardType cardType;

    public Card(String type) {
        switch (type.toUpperCase()) {
            case ("GOLD"):
                cardType = CardType.GOLD;
                break;
            case ("BONUS"):
                cardType = CardType.BONUS;
                break;
            case ("SOCIAL"):
                cardType = CardType.SOCIAL;
                break;
            case ("USUAL"):
                cardType = CardType.USUAL;
                break;
            default:
               cardType = CardType.USUAL;
        }
    }
}
