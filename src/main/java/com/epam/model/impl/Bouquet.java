package com.epam.model.impl;

import com.epam.model.impl.flower.Flower;
import com.epam.model.impl.event.Event;

import java.util.ArrayList;
import java.util.List;

public class Bouquet {

    private List<Flower> flowers;
    private int price;

    public Bouquet() {
        flowers = new ArrayList<>();
    }

    public void chooseBouquet(Event event, String keyWord, Bouquet bouquet, String keyMenu){
        if (keyWord.equals("1")) {
            event.makeSimpleBouquet(bouquet);
        }
        if (keyWord.equals("2")) {
            event.makeOrdinaryBouquet(bouquet);
        }
        if (keyWord.equals("3")) {
            event.makeHugeBouquet(bouquet);
        }
        if (keyWord.equals("4")) {
            event.makeCustomBouquet(keyMenu, bouquet, event);
        }
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public int getPrice() {
        return price;
    }

    public void addPrice(int price) {
        this.price += price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
