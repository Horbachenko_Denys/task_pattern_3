package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    public static Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner scanner = new Scanner(System.in);
    private String keyMenu;
    private Controller controller;
    private Map<String, String> generalMenu;
    private Map<String, String> eventMenu;
    private Map<String, String> bouquetChooseMenu;
    private Map<String, String> createCustomBouquetMenu;
    private Map<String, Printable> generalMenuMethods;

    public MyView() {
        controller = new ControllerImpl();
        generalMenu = new LinkedHashMap<>();
        generalMenu.put("1", " 1 - Make new order...");
        generalMenu.put("2", " 2 - Read new offer...");
        generalMenu.put("3", " 3 - Add new customer...");
        generalMenu.put("4", " 4 - Show customer list...");
        generalMenu.put("Q", " Q - Exit");

        this.generalMenuMethods = new LinkedHashMap<>();
        generalMenuMethods.put("1", this::pressGeneralButton1);
        generalMenuMethods.put("2", this::pressGeneralButton2);
        generalMenuMethods.put("3", this::pressGeneralButton3);
        generalMenuMethods.put("4", this::pressGeneralButton4);
        generalMenuMethods.put("Q", this::pressGeneralButtonQuit);
        do {
            showGeneralMenu();
            System.out.println("Please, select menu point...");
            keyMenu = scanner.nextLine().toUpperCase();
           try { generalMenuMethods.get(keyMenu).print();
           } catch (NullPointerException e) {
               System.out.println("Uncorrected input, try again...");
               e.printStackTrace();
           }
        } while (!keyMenu.equals("Q"));
    }

    private void showGeneralMenu() {
        System.out.println("\n===== General Menu =====");
        for (String str : generalMenu.values()) {
            System.out.println(str);
        }
    }

    private void pressGeneralButton1() {
        showEventMenu();
        keyMenu = scanner.nextLine();
        controller.chooseEvent(keyMenu);

        showBouquetChooseMenu();
        keyMenu = scanner.nextLine();
        if (keyMenu.equals("4")) {
            showCreateCustomBouquetMenu();
            controller.createCustomBouquet(keyMenu);
            do {
                System.out.println("Choose flower or next");
                keyMenu = scanner.nextLine().toUpperCase();
                if (!keyMenu.equals("N")) {
                   controller.chooseCustomFlower(keyMenu);
                }
            } while (!keyMenu.equals("N"));
            System.out.println("Enter client id...");
        } else {
        controller.chooseBouquet(keyMenu);
        }


        keyMenu = scanner.nextLine();
        controller.identifyClient(keyMenu);

        System.out.println(controller.showOrder());
    }

    private void pressGeneralButton2() {
        controller.showNewOffers();
    }

    private void pressGeneralButton3() {
        controller.addNewCustomer();
    }

    private void pressGeneralButton4() {
        controller.showAllCustomers();
    }

    private void pressGeneralButtonQuit() {
    }

    private void showEventMenu() {
        System.out.println("\n===== Event Menu =====");
        eventMenu = new LinkedHashMap<>();
        eventMenu.put("1", " 1 - Choose wedding bouquet...");
        eventMenu.put("2", " 2 - Choose birthday bouquet...");
        eventMenu.put("3", " 3 - Choose St.Valentines Day bouquet...");
        eventMenu.put("4", " 4 - Choose funeral bouquet...");
        eventMenu.put("Q", " Q - Exit");
        for (String str : eventMenu.values()) {
            System.out.println(str);
        }
    }

    private void showBouquetChooseMenu() {
        System.out.println("\n===== Bouquet choose Menu =====");
        bouquetChooseMenu = new LinkedHashMap<>();
        bouquetChooseMenu.put("1", " 1 - Choose simple bouquet...");
        bouquetChooseMenu.put("2", " 2 - Choose ordinary bouquet...");
        bouquetChooseMenu.put("3", " 3 - Choose huge bouquet...");
        bouquetChooseMenu.put("4", " 4 - Choose custom bouquet ...");
        bouquetChooseMenu.put("Q", " Q - Exit");

        for (String str : bouquetChooseMenu.values()) {
            System.out.println(str);
        }
    }

        private void showCreateCustomBouquetMenu() {
            System.out.println("\n===== Bouquet choose Menu =====");
            createCustomBouquetMenu = new LinkedHashMap<>();
            createCustomBouquetMenu.put("1", " 1 - Add Chrysanthemum...");
            createCustomBouquetMenu.put("2", " 2 - Add Marigold...");
            createCustomBouquetMenu.put("3", " 3 - Add Rose...");
            createCustomBouquetMenu.put("4", " 4 - Add Tulip ...");
            createCustomBouquetMenu.put("N", " N - Next");

            for (String str : createCustomBouquetMenu.values()) {
                System.out.println(str);
            }
    }
}
